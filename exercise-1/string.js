// Ucfirst
function ucfirst(str){

    if(typeof str !== "string" || !str) return "";

    return str[0].toUpperCase() + str.substring(1);
}


// Capitalize
function capitalize(str){
    if(typeof str !== "string" || !str) return "";  

    return str.toLowerCase().split(" ").map(function(item){
            return ucfirst(item);
    }).join(" ");

}

// CamelCase
function camelCase(str){
    if(typeof str !== "string" || !str) return "";  

    return capitalize(str).replace(/ /g, "");
}

//snake_case
function snake_case(str){
	if(typeof str !== "string" || !str) return "";  

	return str.toLowerCase().trim(" ").replace(/ /g, "_");
}


